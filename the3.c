/*
2036234 İsmail Tüzün
2036408 Yunus Emre Zengin 
*/

#include <p18cxxx.h>
#include <p18f8722.h>
#pragma config OSC = HSPLL, FCMEN = OFF, IESO = OFF, PWRT = OFF, BOREN = OFF, WDT = OFF, MCLRE = ON, LPT1OSC = OFF, LVP = OFF, XINST = OFF, DEBUG = OFF

#define _XTAL_FREQ   40000000

#include "Includes.h"
#include "LCD.h"


typedef struct product{
    char* pid;
    char* pname;
    int C;
    int price;
    int maxRange;
} product;

typedef struct category{
    char* cname;
    product* products[4];
    int maxRange;
} category;

category categories[4];
product products[16];
unsigned char c1;
unsigned int timer0counter;
unsigned int Timer0counterShadow;
unsigned int tmr0shadow;
int tmr0Flag = 0;
unsigned int timer1counter;
unsigned int Timer1counterShadow;
unsigned long int tmr1shadow;
unsigned long potVal;
unsigned int tmp;
unsigned int time = 0;
int blinkFlag = 0;
int portBpressed;
int activeLine = 0;
int activeRange = 5;
int activeRangeP = 5;
int activeCategory = 0;
int activeProduct = 0;
int rb6Flag = 0;
int tmr1secCounter = 20;
int tmr1secCounterShadow = 20;
int secPassFlag = 0;
int secPass = 0;
int currentId;
int id1;
int id10;
int credit;
int remainingCredit;
int tmr0secFlag = 0;
int tmr0secCounterShadow = 0;
int tmr0secCounter = 0;

void updateLCDpotVal();
void initCategories();
void waitRE1button();
void showCategory();
void showProduct();
void lighten7_segment(int, int, int, int);
void greeting();
void blink();
void selectionStep();
void creditStep();
void purchaseStep();

void initConf(){
    TRISH4 = 1;                     // A/D port input setting
    ADCON0 = 0b00110000;            // set chanel 1100 => chanel12 
    ADCON1 = 0b00000000;            // set voltage limits and set analog all.
    ADCON2 = 0b10001111;            // right justified
    PIE1bits.ADIE = 1;              // A/D interrupt enable
    
    INTCONbits.TMR0IE = 1;          // enable TMR0 interrupts
    INTCONbits.TMR0IF = 0;          // clear timer0 interrupt flag

    TRISB6 = 1;                     // set RB6 as input pin to use PORTB interrupt
    TRISB7 = 1;                     // set RB7 as input pin to use PORTB interrupt
    PORTB = 0;                      // clear PORTB in order to avoid unexpected situations
    LATB = 0;                       // clear LATB in order to avoid unexpected situations
    INTCONbits.RBIE = 1;            // enable PORTB change interrupts
    INTCONbits.RBIF = 0;            // clear PORTB interrupt flag
    INTCON2bits.RBPU = 0;           // PORTB pull-ups are enabled by individual port latch values

    T0CON = 0b01000111;             // set pre-scaler 1:256, use timer0 as 8-bit
    Timer0counterShadow = 20;       // counter for timer0 100ms (1000000 instruction => 256 * 20 *(256-60) )
    timer0counter = Timer0counterShadow;     // setTimer0Counter
    tmr0shadow = 60;                // timer0 initial value configuration for 100ms
    TMR0 = tmr0shadow;              // set timer0's initial value
    
    TRISE1 = 1;                     // set RE1 as input pin



    TMR1IE = 1;                     // enable timer1 interrupts
    T1CON = 0b10110000;             // use 16-bit mode, set pre-scaler 1:8
    Timer1counterShadow = 5;        // set timer1 initial value configuration for 250ms
    timer1counter = Timer1counterShadow;     // setTimer1Counter for 250ms
    tmr1shadow = 3036;              // counter for timer1 250ms (2500000 instruction => 8 * 5 *(2^16-3036) )
    TMR1 = tmr1shadow;              // set timer1's initial value
    tmr1secCounterShadow = 20;      // set timer1 initial value configuration for 1 sec
    tmr1secCounter = tmr1secCounterShadow;  // set timer1 counter for 1 sec
    TRISJ = 0;                      // Seven segment display configures for output
    LATJ = 0;                       // clear LATJ in order to avoid unexpected situations
    LATH = 0;                       // clear LATH in order to avoid unexpected situations
    TRISH = TRISH & 0b11110000;     // set PORTH<3:0> as output

    tmr0secCounterShadow = 200;     // set timer0 initial value configuration for 1 sec
    tmr0secCounter = tmr0secCounterShadow;  // set timer0 counter for 1 sec
    tmr0Flag = 0;                   // clear flag for initialization

}

void interrupt ISR(void){

    if(TMR0IE && TMR0IF){               // timer0 interrupt comes
        TMR0 = tmr0shadow;              // set timer0's initial value
        if(--timer0counter == 0){       //timer0 has arrived set its initial values and restart again
            timer0counter = Timer0counterShadow; //reset timer0counter to it is initial value
            tmr0Flag = 1;               // set tmr0flag to 1
            ADCON0bits.GO_DONE=1;       // start an A/D conversion

        }
        if(--tmr0secCounter == 0){      // check tmr0secCounter for to find out a sec passed
            tmr0secCounter = tmr0secCounterShadow; // set tmr0secCounter to its initial value
            tmr0secFlag = 1;            // set tmr0secFlagto 1 in order to approve 1 sec passed
        }

        TMR0IF = 0;                     // clear interrupt flag
        return;

    }else if(ADIE && ADIF){             // A/D interrupt comes
        potVal = ADRES;                 // Every time the A/D conversion finishes write the result into potVal
        ADIF = 0;                       // clear interrupt flag
        return;
    }else if(RBIE && RBIF){             // RB port change interrupt comes
        tmp = PORTB;                    // read PORTB in order to avoid mismatch condition
        RBIF = 0;                       // clear interrupt flag
        
        // By this if-else we fire PORTB dependent events on button release
        if(portBpressed == 0){
            if(!PORTBbits.RB6){
                portBpressed = 1;       // save pressed button
            }else if(!PORTBbits.RB7){
                portBpressed = 2;       // save pressed button
            }
        }else if(portBpressed == 1){    // PORTB6 released
            rb6Flag = 1;
            portBpressed = 0;
        }else if(portBpressed == 2){    // PORTB7 released
            activeLine = !activeLine;
            portBpressed = 0;
        }
        return;

    }else if(TMR1IE && TMR1IF){                     // timer1 interrupt comes
        TMR1 = tmr1shadow;                          // set timer1's initial value
        if(--timer1counter == 0){                   // timer1 has arrived set its initial values and restart again
            timer1counter = Timer1counterShadow;    // set timer1counterto its initialvalue
            blinkFlag = !blinkFlag;                 // toggle blink flag everytime for active line pointer (turn-on, turn-of, turn-on, turn-of, ...)
        }
        if(--tmr1secCounter == 0){                  // check tmr1secCounter for to find out a sec passed
            tmr1secCounter = tmr1secCounterShadow;  // timer1 has arrived set its initial values and restart again
            secPassFlag = 1;                        // set secPassFlag to 1 in order to confirm a second passed
        }
        
        TMR1IF = 0;                                 // clear interrupt flag
        return;
    }
    
    return;                                         // this code should never be executed!!!
}

void main(void){
    initConf();                     // make initialization
    initCategories();               // set initial stock and create categories

    __delay_ms(15);
    __delay_ms(15);
    __delay_ms(15);
    __delay_ms(15);

    InitLCD();                      // initialize LCD in 4bit mode
    ClearLCDScreen();               // Clear LCD screen

    waitRE1button();                // wait for user to press RE1 button here,
                                    // at that time set 7-segment display
    greeting();                     // welcome user

    ADON = 1;                       // enable A/D conversion module
    ADIF = 0;                       // clear A/D interrupt flag
    ADIE = 1;                       // enable A/D interrupts
    INTCONbits.GIE_GIEH = 1;        // enable global interrupts
    INTCONbits.PEIE_GIEL = 1;       // enable peripheral interrupts

    // start timer0 and timer1 here
    TMR0ON = 1;
    TMR1ON = 1;
    
    while(1){                           // main loop runs forever :)
        lighten7_segment(10,10,10,10);  // lighten 7-segment display as [-][-][-][-]
        blink();                        // call cursor blink function
        
        if(activeLine == 1){            // use potentiometer for products/category
            showProduct();              // by looking at active line
        }else{                          //
            showCategory();             //
        }
        
        if(rb6Flag){                    // RB6 button released
            rb6Flag = 0;                // clear flag for next usage
            currentId = activeCategory*4 + activeProduct + 1;   // calculate and save active product id

            if(activeLine && products[currentId - 1].C){        // give permission to buy product
                selectionStep();                                // if user selected the product or
                creditStep();                                   // vending machine has that product
                purchaseStep();                                 // Then process step by step
                greeting();                                     // at the end prepare vending machine
                waitRE1button();                                // next trade
            }
        }
        
      
    }
}

void waitRE1button(){
    RBIE = 0;                           // Disable PORTB interrupts

    while(PORTEbits.RE1){               // Busy wait for RE1 press
        lighten7_segment(10,10,10,10);  //meanwhile lighten 7-segment display
    }
    while(!PORTEbits.RE1){              // Busy wait for RE1 relase
        lighten7_segment(10,10,10,10);  //meanwhile lighten 7-segment display
    }
    ClearLCDScreen();                   // Clear LCD completely
    RBIE = 1;                           // Enable PORTB interrupts

}

void lighten7_segment(int d0, int d1, int d2, int d3){
    LATH = LATH & 0xF0;
    int sym[]={
        0b00111111,                     // 7-Segment = 0
        0b00000110,                     // 7-Segment = 1
        0b01011011,                     // 7-Segment = 2
        0b01001111,                     // 7-Segment = 3
        0b01100110,                     // 7-Segment = 4
        0b01101101,                     // 7-Segment = 5
        0b01111101,                     // 7-Segment = 6
        0b00000111,                     // 7-Segment = 7
        0b01111111,                     // 7-Segment = 8
        0b01100111,                     // 7-Segment = 9
        0b01000000,                     // 7-Segment = -
        0b00000000                      // 7-Segment = turn off
    };

    LATJ = sym[d3];                     // Set LATJ to d3 parameter
    LATH3 = 1;                          // Turn on D3 of 7-segment display
    __delay_us(500);                    // wait for shortly
    LATH3 = 0;                          // Turn on D3 of 7-segment display

    LATJ = sym[d2];                     // Set LATJ to d2 parameter
    LATH2 = 1;                          // Turn on D2 of 7-segment display
    __delay_us(500);                    // wait for shortly
    LATH2 = 0;                          // Turn on D2 of 7-segment display

    LATJ = sym[d1];                     // Set LATJ to d1 parameter
    LATH1 = 1;                          // Turn on D1 of 7-segment display
    __delay_us(500);                    // wait for shortly
    LATH1 = 0;                          // Turn on D1 of 7-segment display

    LATJ = sym[d0];                     // Set LATJ to d0 parameter
    LATH0 = 1;                          // Turn on D0 of 7-segment display
    __delay_us(500);                    // wait for shortly
    LATH0 = 0;                          // Turn on D0 of 7-segment display

    return;
}


void greeting(){                            // welcome user  
    WriteCommandToLCD(0x80);                // set position first line to write
    WriteStringToLCD("  Ceng Vending  ");   // write to first line
    WriteCommandToLCD(0xC0);                // set position second line to write
    WriteStringToLCD(" $$$Machine$$$  ");   // write to second line
    unsigned long int wait3sec = 1486;      // set busy wait condition to 3 second
    while(--wait3sec){                      // busy wait
        lighten7_segment(10,10,10,10);      // while waiting don't ignore 7-segment display otherwise it will be turn off

    }
}

void initCategories(){

   // This function fills initial data to structs
   // given in the pdf file of the homework.

   category* c;
   product *p0, *p1, *p2, *p3;

   c = &categories[0];
   c->cname = "Beverages";
   c->maxRange = 250;

   p0 = &products[0];
   p0->pid = "01";
   p0->pname = "Water";
   p0->C = 9;
   p0->price = 2;
   p0->maxRange = 250;

   p1 = &products[1];
   p1->pid = "02";
   p1->pname = "Milk";
   p1->C = 2;
   p1->price = 3;
   p1->maxRange = 500;

   p2 = &products[2];
   p2->pid = "03";
   p2->pname = "Coke";
   p2->C = 3;
   p2->price = 5;
   p2->maxRange = 750;

   p3 = &products[3];
   p3->pid = "04";
   p3->pname = "Tea";
   p3->C = 1;
   p3->price = 4;
   p3->maxRange = 1024;

   c->products[0] = p0;
   c->products[1] = p1;
   c->products[2] = p2;
   c->products[3] = p3;





   c = &categories[1];
   c->cname = "Snack Food";
   c->maxRange = 500;

   p0 = &products[4];
   p0->pid = "05";
   p0->pname = "Seeds";
   p0->C = 3;
   p0->price = 5;
   p0->maxRange = 250;

   p1 = &products[5];
   p1->pid = "06";
   p1->pname = "Nuts";
   p1->C = 1;
   p1->price = 4;
   p1->maxRange = 500;

   p2 = &products[6];
   p2->pid = "07";
   p2->pname = "Chips";
   p2->C = 2;
   p2->price = 3;
   p2->maxRange = 750;

   p3 = &products[7];
   p3->pid = "08";
   p3->pname = "PCorn";
   p3->C = 3;
   p3->price = 5;
   p3->maxRange = 1024;

   c->products[0] = p0;
   c->products[1] = p1;
   c->products[2] = p2;
   c->products[3] = p3;





   c = &categories[2];
   c->cname = "Fast Food";
   c->maxRange = 750;

   p0 = &products[8];
   p0->pid = "09";
   p0->pname = "Pizza";
   p0->C = 2;
   p0->price = 3;
   p0->maxRange = 250;

   p1 = &products[9];
   p1->pid = "10";
   p1->pname = "Hambu";
   p1->C = 3;
   p1->price = 5;
   p1->maxRange = 500;

   p2 = &products[10];
   p2->pid = "11";
   p2->pname = "Sandw";
   p2->C = 1;
   p2->price = 4;
   p2->maxRange = 750;

   p3 = &products[11];
   p3->pid = "12";
   p3->pname = "Crack";
   p3->C = 0;
   p3->price = 4;
   p3->maxRange = 1024;

   c->products[0] = p0;
   c->products[1] = p1;
   c->products[2] = p2;
   c->products[3] = p3;




   c = &categories[3];
   c->cname = "Desert";
   c->maxRange = 1024;

   p0 = &products[12];
   p0->pid = "13";
   p0->pname = "Cake";
   p0->C = 3;
   p0->price = 5;
   p0->maxRange = 250;

   p1 = &products[13];
   p1->pid = "14";
   p1->pname = "Choc";
   p1->C = 1;
   p1->price = 4;
   p1->maxRange = 500;

   p2 = &products[14];
   p2->pid = "15";
   p2->pname = "ICrea";
   p2->C = 9;
   p2->price = 2;
   p2->maxRange = 750;

   p3 = &products[15];
   p3->pid = "16";
   p3->pname = "Biscu";
   p3->C = 2;
   p3->price = 3;
   p3->maxRange = 1024;

   c->products[0] = p0;
   c->products[1] = p1;
   c->products[2] = p2;
   c->products[3] = p3;

}

void showCategory(){
    activeRangeP = 5;

    category *c;                        // Create a temporary cateory pointer
    product *p;                         // Create a temporary product pointer
    
    int i;                              // a variable to store gap index
    if(potVal <= 250){
        i = 0;                          // we are in 0th gap
    }else if(potVal <= 500){
        i = 1;                          // we are in 1st gap
    }else if(potVal <= 750){
        i = 2;                          // we are in 2nd gap
    }else{
        i = 3;                          // we are in 3rd gap
    }

    c = &categories[i];                 // select category from category array according to gap
    if(activeRange != i){               // this checks for if gap changed (ex: gap0 => gap1)
        ClearLine0();                   // Clear LCD screen's first line except first char
        ClearLine1();                   // Clear LCD screen's second line except first char
    }
    activeRange = i;                    // update activeRage (this value would store the previous value in next comparison)
    activeCategory = i;                 // update the variabe in order to remember selected category

    p = (c->products[0]);               // chose 0th product of category in order to print to LCD

    // Write required information to LCD according to homework requirements
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteCommandToLCD(0x81);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(c->cname);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteCommandToLCD(0xC0);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(" ");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(p->pid);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD("-");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(p->pname);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(" C:");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteDataToLCD((char)(48 + p->C));
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD("$:");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteDataToLCD((char)(48 + p->price));



}

void blink(){
    char * c;
    // check if user tries to buy non-existing product if so change cursor to 'X'
    if(activeLine == 1 && products[currentId - 1].C == 0){
        c = "X";
    }else{
        c = ">";
    }
    
    // set position to write with respect to active line
    if(activeLine == 0){
        WriteCommandToLCD(0x80);
    }else{
        WriteCommandToLCD(0xC0);
    }
    
    // show or hide cursor with respect to blinkFlag which is arranged at ISR
    if(blinkFlag){
        WriteStringToLCD(c);
    }
    else{
        WriteStringToLCD(" ");
    }

}

void showProduct(){
    activeRange = 5;

    category *c;                        // Create a temporary cateory pointer
    product *p;                         // Create a temporary product pointer
    c = &categories[activeCategory];    // set category pointer to active category
    currentId = activeCategory*4 + activeProduct + 1;   // compute current products ID
    int i;                              // a variable to store gap index
    if(potVal <= 250){
        i = 0;                          // we are in 0th gap
    }else if(potVal <= 500){
        i = 1;                          // we are in 1st gap
    }else if(potVal <= 750){
        i = 2;                          // we are in 2nd gap
    }else{
        i = 3;                          // we are in 3rd gap
    }
    if(activeRangeP != i){              // this checks for if gap changed (ex: gap0 => gap1)
        ClearLine1();                   // Clear LCD screen's first line except first char
    }
    activeRangeP = i;                   // update activeRageP (this value would store the previous value in next comparison)
    activeProduct = i;                  // update the variabe in order to remember selected product

    p = (c->products[i]);

    // Write required information to LCD according to homework requirements
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteCommandToLCD(0x80);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(" ");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteCommandToLCD(0xC1);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(p->pid);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD("-");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(p->pname);
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD(" C:");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteDataToLCD((char)(48 + p->C));
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteStringToLCD("$:");
    lighten7_segment(10,10,10,10);      // Turn on 7-segment display
    WriteDataToLCD((char)(48 + p->price));

}

void selectionStep(){
    id1 = currentId % 10;                   // set units digit for 7-segment display
    id10 = currentId / 10;                  // set tens digit for 7-segment display
    activeLine = 1;                         // change active line to second line
    secPass = 0;                            // initialize secPass that stores how many second passed
    secPassFlag = 0;                        // initialize secPassFlag that shows that one second elapsed 
    tmr1secCounter = tmr1secCounterShadow;  // initialize counter for timer1
    while(secPass < 3){                     // stop after 3 seconds
        blink();                            // blink function for cursor
       
        // toggle 7-segment display     D1,D0
        if(secPass % 2){ // odd values of secPass means turn off 7-segment D1,D0
            lighten7_segment(0, 0, 11, 11);     // 7-segment => [0][0][][]
        } else{ // even values of sec pass means turn on 7-segment D1,D0
            lighten7_segment(0, 0, id10, id1);  // 7-segment => [0][0][id10][id]
        }
        
        if(secPassFlag){                    // if one second elapsed this flag will be 1
            secPassFlag = 0;                // clear it for next second
            secPass += 1;                   // increment second passed
        }
    }
}

void creditStep(){
    activeLine = 0;                         // set active line as first line
    WriteCommandToLCD(0x81);                // goto to the beginning of the first line, give space to the cursor
    WriteStringToLCD("Enter Credits  ");    // write to the first line 
    WriteCommandToLCD(0xC0);                // goto to the beginning of the second line
    WriteStringToLCD(" ");                  // clear first char of the second line
    secPass = 0;                            // initialize secPass that stores how many second passed
    secPassFlag = 0;                        // initialize secPassFlag that shows that one second elapsed
    tmr1secCounter = tmr1secCounterShadow;  // initialize counter for timer1
   
    while(secPass < 4){                     // stop after 4 seconds
        blink();                            // blink function for cursor

        // toggle 7-segment display     D3
        if(secPass % 2){ // odd values of secPass means turn off 7-segment D3
            lighten7_segment(11, 0, id10, id1); // 7-segment => [][0][id10][id1]
        } else{ // even values of sec pass means turn on 7-segment D3
            lighten7_segment(0, 0, id10, id1);  // 7-segment => [0][0][id10][id1]
        }
        
        if(secPassFlag){                    // if one second elapsed this flag will be 1
            secPassFlag = 0;                // clear it for next second
            secPass += 1;                   // increment second passed
        }
    }

    while(!rb6Flag){                        // here potentiometer should change credit
        blink();                            // blink function for cursor
        credit = potValToCredit();          // get value from potentiometer and set credit
        lighten7_segment(credit, 0, id10, id1); // don't forget to lighten 7-segment display 
    }
    rb6Flag = 0;                            // clear flag for next usage
}

void purchaseStep(){
    activeRange = 5;                // change active range for category so that the next usage first line will be cleared
    activeRangeP = 5;               // change active range for product so that the next usage second line will be cleared



    category *c = &categories[activeCategory];  // set c to active category
    product *p = (c->products[activeProduct]);  // set p to active product
    p->C -= 1;                                  // decrease the count of product
    remainingCredit = credit - p->price;        // calculate remaining credit

    ClearLine0();                   // clear LCD first line
    // write necessary information to LCD
    WriteCommandToLCD(0x80);
    WriteStringToLCD(" ");
    WriteStringToLCD(c->cname);

    WriteCommandToLCD(0xC1);
    WriteStringToLCD(p->pid);
    WriteStringToLCD("-");
    WriteStringToLCD(p->pname);
    WriteStringToLCD(" C:");
    WriteDataToLCD((char)(48 + p->C));
    WriteStringToLCD("$:");
    WriteDataToLCD((char)(48 + p->price));


    activeLine = 0;                             // set active line as first line
    secPass = 0;                                // initialize secPass that stores how many second passed
    tmr0secCounter = tmr0secCounterShadow;      // initialize counter for timer0
    tmr0secFlag = 0;                            // initialize tmr0secFlag that shows that one second elapsed
    while(secPass < 3){                         // stop after 3 seconds
        blink();                                // blink function for cursor

        // toggle 7-segment display     D2
        if(secPass % 2){ // odd values of secPass means turn off 7-segment D2
            lighten7_segment(credit, 11, id10, id1);    // 7-segment => [credit][][id10][id1]
        } else{ // even values of sec pass means turn on 7-segment D2
            lighten7_segment(credit, remainingCredit, id10, id1);   // 7-segment => [credit][remainingCredit][id10][id1]
        }


        if(tmr0secFlag){                        // if one second elapsed this flag will be 1
            secPass++;                          // increment second passed
            tmr0secFlag = 0;                    // clear it for next second
        }
    }

}

int potValToCredit(){
    // return credit value from potentiometer with associative intervals
    if(potVal < 102){
        return 0;
    }else if(potVal < 204){
        return 1;
    }else if(potVal < 306){
        return 2;
    }else if(potVal < 408){
        return 3;
    }else if(potVal < 510){
        return 4;
    }else if(potVal < 612){
        return 5;
    }else if(potVal < 714){
        return 6;
    }else if(potVal < 816){
        return 7;
    }else if(potVal < 918){
        return 8;
    }else {
        return 9;
    }
}
